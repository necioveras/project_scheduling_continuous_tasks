package datasets;

import heuristics.AnnealingSimulated;
import heuristics.RandomScheduling;

import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

import model.Area;
import model.Member;
import model.Task;
import model.Team;
import schedules.MemberMonthlySchedule;
import schedules.TeamMonthlySchedule;
import useful.Definitions;
import useful.FileRead;

public class RandomDataSet {

	private String fileOutPut = "randomDataSet.txt";
	private double data_indexes[][]; 
	private double data_goals[][];
	private double data_bulkWorked[][];
	private double data_memberProductivity[][][];																																					//A produtividade de cada tarefa em cada mês para cada desenvolvedor
	private double data_qualityTeam[];
	private double data_qualityMembers[][];                                    																											//A qualidade de cada mês para cada desenvolvedor


	/* ***************************************************************************************
	 *      						       															Métodos Públicos  			 												         * 				
	 * *************************************************************************************** */

	public RandomDataSet(String outputFile){
		fileOutPut = outputFile;
	}

	public void execute(String fromFile, double percentEvolutionGoals){

		Team team = new Team();	//Novo Time
		Task tasks[] = new Task[0];	//Array de Tarefas
		Area areas[] = new Area[0]; //Array de Areas		

		try{			
			//Carregar o arquivo
			FileRead readed_file = new FileRead(fromFile);
			team = readed_file.readMembers();
			tasks = readed_file.readTasks();
			areas = readed_file.readAreas();
			readed_file.setAvailableTasksToMembers();
			readed_file.closefile();			
		} catch (IOException i) { i.printStackTrace();}

		data_indexes = new double [tasks.length][Definitions.VALUE_EXEC_MONTHS+1];                                                         //Mais UM, pois o último índice indica a média
		data_goals = new double [tasks.length][Definitions.VALUE_EXEC_MONTHS];  
		data_bulkWorked = new double [tasks.length][Definitions.VALUE_EXEC_MONTHS];
		data_memberProductivity = new double [team.getMembers().size()][tasks.length][Definitions.VALUE_EXEC_MONTHS];
		data_qualityTeam = new double[Definitions.VALUE_EXEC_MONTHS+1];
		data_qualityMembers = new double[team.getMembers().size()][Definitions.VALUE_EXEC_MONTHS+1];

		for (int i = 0; i < Definitions.VALUE_EXEC_MONTHS; i++){                                         																											//controle dos MESES

			double avgIndexes[] = new double[tasks.length];													//soma para cálculo das médias dos indices de cada tarefa												
			double avgGoals[] = new double[tasks.length];														//soma para cálculo das médias dos objetivos de cada tarefa
			double avgBulkWorked[] = new double[tasks.length];
			double avgProductivity[][] = new double[team.getMembers().size()][tasks.length];
			double avgQualityTeam = 0;
			double avgQualityMembers[] = new double[team.getMembers().size()];

			for (int j = 0; j < Definitions.VALUE_EXEC_DATASETS; j++){                             																											//controle das EXECUÇÕES
				TeamMonthlySchedule monthSchedule = RandomScheduling.createRandomScheduling(team, areas);
				monthSchedule.updateCalcsQuality(team, areas);
				//HillClimbing.exec(monthSchedule, areas, 100);

				//AnnealingSimulated.exec(monthSchedule, areas, 100, 0.8, 1, 0.0001, 180, 10);
				AnnealingSimulated.exec(monthSchedule, areas, 100, 0.8, 1, 0.0001, 1000, 20);

				for (Task t: tasks){																													//Construção do somatório para cada mês
					avgIndexes[t.getId()-1] += t.getIndex();
					avgGoals[t.getId()-1] += t.getGoal();
					avgBulkWorked[t.getId()-1] += t.getBulkWorked();

					for (Member m: team.getMembers())																		//A participação de cada membro em relação à cada tarefa
						for (MemberMonthlySchedule monthMember: monthSchedule.getTimeTable().values())
							if (monthMember.getMember().equals(m))
								avgProductivity[m.getId()-1][t.getId()-1] += monthMember.getBulkWorked(t);								
				}
				for (Member m: team.getMembers())																		//Captura da qualidade das agendas individuais
					for (MemberMonthlySchedule monthMember: monthSchedule.getTimeTable().values())
						if (monthMember.getMember().equals(m))
							avgQualityMembers[m.getId()-1] += monthMember.getQuality(); 

				avgQualityTeam += monthSchedule.getQuality();
			}	

			for (Task t: tasks){																													//Obtenção da média para cada mês (final das X execuções)
				data_indexes[t.getId()-1][i] = avgIndexes[t.getId()-1] / (double) Definitions.VALUE_EXEC_DATASETS;
				data_goals[t.getId()-1][i] = avgGoals[t.getId()-1] / (double) Definitions.VALUE_EXEC_DATASETS;
				data_bulkWorked[t.getId()-1][i] = avgBulkWorked[t.getId()-1] / (double) Definitions.VALUE_EXEC_DATASETS;
				for (Member m: team.getMembers()){
					data_memberProductivity[m.getId()-1][t.getId()-1][i] = avgProductivity[m.getId()-1][t.getId()-1] / (double) Definitions.VALUE_EXEC_DATASETS;
					data_qualityMembers[m.getId()-1][i] = avgQualityMembers[m.getId()-1] / (double) Definitions.VALUE_EXEC_DATASETS;
				}
			}

			data_qualityTeam[i] = avgQualityTeam / (double) Definitions.VALUE_EXEC_DATASETS;			
			
			//evolução das metas em 8%
			for (Area a: areas)
				a.evolutionGoals(percentEvolutionGoals);

		}

		calcAveragesForAverages();		
		generateOutput(fileOutPut, tasks, team.getMembers());		

	}	

	public void generateOutput(String file, Task tasks[], List<Member > members){
//		outputTasksIndexesLines(tasks, file);
//		outputWorkDataLines(tasks, members, file);
//		outputWorkQualityLines(members, file);
		outputAllDataCollumns(file, tasks, members);
	}

	/* ***************************************************************************************
	 *      						       															Métodos Privados  			 												         * 				
	 * *************************************************************************************** */	
	private void calcAveragesForAverages(){
		double avgIndexes = 0, avgQualityTeam = 0, avgQualityMember = 0;

		for (int i = 0; i < data_indexes.length; i++){                   //cada tarefa
			for (int j = 0; j < data_indexes[i].length-1; j++)         //os respectivos indices
				avgIndexes += data_indexes[i][j];
			data_indexes[i][data_indexes[i].length-1] = avgIndexes / (double) Definitions.VALUE_EXEC_MONTHS;
			avgIndexes = 0;
		}

		for (int i = 0; i < data_qualityTeam.length-1; i++) 
			avgQualityTeam += data_qualityTeam[i];
		data_qualityTeam[data_qualityTeam.length-1] = avgQualityTeam / (double) Definitions.VALUE_EXEC_MONTHS;

		for (int i = 0; i < data_qualityMembers.length; i++) {									//Cada membro
			for (int j = 0; j < data_qualityMembers[i].length; j++)                    //Suas respectivas qualidades 
				avgQualityMember += data_qualityMembers[i][j];
			data_qualityMembers[i][data_qualityMembers[i].length-1] = avgQualityMember / (double) Definitions.VALUE_EXEC_MONTHS;
			avgQualityMember = 0;
		}
	}


	private void outputTasksIndexesLines(Task tasks[],  String file){
		try{

			FileWriter fw = new FileWriter(file);	
			fw.write("#tasks indexes");				
			for (Task t: tasks){
				fw.write("\nT" + t.getId());
				for (int i = 0; i < data_indexes[t.getId()-1].length; i++)                          //O índice em cada mês
					fw.write("\t" + data_indexes[t.getId()-1][i]);
				fw.write("\nG" + t.getId());
				for (int i = 0; i < data_goals[t.getId()-1].length; i++)						//O objetivo em cada mês
					fw.write("\t" + data_goals[t.getId()-1][i]);						
			}				
			fw.close();
		} catch (IOException i) { i.printStackTrace();} 
	}
	
	private void outputWorkDataLines(Task tasks[], List<Member> members,  String file){
		try{

			FileWriter fw = new FileWriter(file, true);	
			fw.write("\n#work data");				
			for (Task t: tasks){
				fw.write("\nT" + t.getId());
				for (int i = 0; i < data_bulkWorked[t.getId()-1].length; i++)                          //O volume de trabalho em cada mês
					fw.write("\t" + data_bulkWorked[t.getId()-1][i]);					
				for (Member m: members){	
					fw.write("\nD"+m.getId());
					for (int j = 0; j < data_memberProductivity[m.getId()-1][t.getId()-1].length; j++) //A produtividade de cada desenvolvedor em cada tarefa
						fw.write("\t" + data_memberProductivity[m.getId()-1][t.getId()-1][j]);
				}											
			}				
			fw.close();
		} catch (IOException i) { i.printStackTrace();} 
	}

	private void outputWorkQualityLines(List<Member>members,  String file){
		try{
			FileWriter fw = new FileWriter(file, true);	
			fw.write("\n#quality");
			fw.write("\nQT");
			for (int i = 0; i < data_qualityTeam.length; i++)
				fw.write("\t" + data_qualityTeam[i]);
			for (Member m: members){
				fw.write("\nD"+ m.getId());
				for (int i = 0; i < data_qualityMembers[m.getId()-1].length; i++)                        //A qualidade de cada membro em cada mês
					fw.write("\t" + data_qualityMembers[m.getId()-1][i]);
			}			
			fw.close();
		} catch (IOException i) { i.printStackTrace();} 
	}
	
	private void outputAllDataCollumns(String file, Task tasks[], List<Member > members){
		try{
			FileWriter fw = new FileWriter(file, false);                            //NOVO arquivo
			//head
			for (Task t: tasks)																										//tasks indexes 
				fw.write("T"+t.getId() + "\t" + "G" + t.getId() + "\t");
			
			for (Task t: tasks){																										//work data
				fw.write("T" + t.getId() + "\t");
				for (Member m: members)
					fw.write("M" + m.getId() + "\t");
			}
			
			fw.write("QT" + "\t");
			for (Member m: members)																						//Qualities
				fw.write("M" + m.getId() + "\t"); 
			
			fw.write("\n");						
			
			BigDecimal ret;
			
			for (int i = 0; i < Definitions.VALUE_EXEC_MONTHS; i++) {           //Linhas de dados (MESES)
				
						for (Task t: tasks){																						//dados para 'tasks indexes'
							    ret = new BigDecimal(data_indexes[t.getId()-1][i]).setScale(2, RoundingMode.DOWN);
								fw.write(ret  + "\t");
								ret = new BigDecimal(data_goals[t.getId()-1][i]).setScale(2, RoundingMode.DOWN);
								fw.write(ret + "\t");						
						}	
						
						for (Task t: tasks){																										//dados para 'work data'
							ret = new BigDecimal(data_bulkWorked[t.getId()-1][i]).setScale(2, RoundingMode.DOWN);
							fw.write(ret + "\t");
							for (Member m: members){
								ret = new BigDecimal(data_memberProductivity[m.getId()-1][t.getId()-1][i]).setScale(2, RoundingMode.DOWN);
								fw.write(ret + "\t");
							}
						}
						
						ret = new BigDecimal(data_qualityTeam[i]).setScale(2, RoundingMode.DOWN);
						fw.write(ret + "\t");
						for (Member m: members){
							ret = new BigDecimal(data_qualityMembers[m.getId()-1][i]).setScale(2, RoundingMode.DOWN);
							fw.write(ret + "\t");
						}
						
						fw.write("\n");
				
			}			
			fw.close();
		} catch (IOException i) { i.printStackTrace();} 
	}

}
