package heuristics;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Random;

import model.Area;
import model.Member;
import model.Task;
import schedules.TeamMonthlySchedule;
import useful.Definitions;

public class AnnealingSimulated {

	private static double flipMonth(Area areas[], int semana, int row, int col, TeamMonthlySchedule monthSchedule, 
			double temperature, Member m, Task t, FileWriter f) throws IOException{

		double quality = monthSchedule.getQuality();
		//guarda tarefa anterior												
		Task taskPrior = monthSchedule.getTimeTable().get(m).getMonthly()[semana].getTasks()[row][col];
		//seta nova tarefa
		monthSchedule.getTimeTable().get(m).getMonthly()[semana].setTask(t, row, col);

		//atualiza os calculos para verificar se qualidade melhorou						
		monthSchedule.updateCalcsQuality(m, areas);

		double deltaQuality = monthSchedule.getQuality() - quality;		

		if (monthSchedule.getQuality() < quality){     //Solução gerada pela vizinhança é pior
			double prob = Math.exp(deltaQuality / temperature); 
			Random random = new Random();
			double x = random.nextDouble();
			if (x < prob){														//aceitar uma solução pior com base em uma probabilidade						
				//f.write(monthSchedule.getQuality() + "\n");
				return deltaQuality;
			}
			else {                //solução gerada não pode ser aceita
				if (taskPrior == null)
					monthSchedule.getTimeTable().get(m).getMonthly()[semana].setTaskNull(row, col);	
				else			
					monthSchedule.getTimeTable().get(m).getMonthly()[semana].setTask(taskPrior, row, col);

				monthSchedule.updateCalcsQuality(m, areas);                 //rollback também nos cálculos de qualidade
				return 0;                                                                                                   //não houve melhoria
			}
		}
		//f.write(monthSchedule.getQuality() + "\n");
		return deltaQuality;
	}


	public static double generateNeighborTeamSchedule(Area areas[], TeamMonthlySchedule monthSchedule, 
			double temperature, FileWriter f) throws IOException{
		Random rCol = new Random(), rRow = new Random(),  rMember = new Random(), rMonthly = new Random(), rArea = new Random(), rTask = new Random();
		int row,col, area, task; 

		//seleciona uma memebro aleatório
		Member m = monthSchedule.getTeam().getMembers().get(rMember.nextInt((int)monthSchedule.getTeam().getSize()));
		//seleciona uma semana Aleatória
		int semana = rMonthly.nextInt(4);

		row = rRow.nextInt(Definitions.MAX_DAYS);
		col = rCol.nextInt(Definitions.MAX_INSTANT);

		area = rArea.nextInt(areas.length);
		task = rTask.nextInt(areas[area].getTasks().size());

		//altera uma tarefa de uma semana				
		if(monthSchedule.getTimeTable().get(m).getMonthly()[semana].getTasks()[row][col] != null){
			if(!monthSchedule.getTimeTable().get(m).getMonthly()[semana].getTasks()[row][col].equals(areas[area].getTasks().get(task))){						
				return flipMonth(areas, semana, row, col, monthSchedule, temperature,  m, areas[area].getTasks().get(task), f);																		
			}
		}
		else{
			return flipMonth(areas, semana, row, col, monthSchedule, temperature, m, areas[area].getTasks().get(task), f);
		}			
		return 0;

		//	double deltaQuality = 0;
		//altera uma tarefa de uma semana
		//		for(Area a: areas){			
		//			for (Task t: a.getTasks()){				
		//				if(monthSchedule.getTimeTable().get(m).getMonthly()[semana].getTasks()[row][col] != null){
		//					if(!monthSchedule.getTimeTable().get(m).getMonthly()[semana].getTasks()[row][col].equals(t)){						
		//						deltaQuality = flipMonth(areas, semana, row, col, monthSchedule, temperature,  m, t, f);																		
		//					}
		//				}
		//				else{
		//					deltaQuality = flipMonth(areas, semana, row, col, monthSchedule, temperature,  m, t, f);
		//				}
		//			}
		//		}
		//		return deltaQuality;
	}

	public static void exec(TeamMonthlySchedule monthSchedule, Area areas[], double temperature, double coolingRate, 
			int maxIterations, double freezing, int maxIterationsfreezing, int maxTemperatureRecovery){	              

		FileWriter f;		

		try {
			f = new FileWriter(new File ("qualidade.txt"));	
			//Solução inicial já foi gerada!

			int count = 0;      //contador de iterações para cada temperatura
			double deterioration = 0;  //estimativa de deteriorização em cada temperatura
			int valueOfRejection = 0;
			int temperatureRecovery = 0;     //controle da quantidade de reaquecimentos

			while (temperature >= freezing && maxIterationsfreezing >= count){
				f = new FileWriter(new File ("qualidade.txt"), true);
				count++;

				double deltaEnergy  = generateNeighborTeamSchedule(areas,monthSchedule, temperature, f);                        //Mudança randômica												
				f.write(monthSchedule.getQuality() + "\n");								
				
				//se a qualtidade de movimentos rejeitados consecutivos for alta então fazer REAQUECIMENTO
				if (deltaEnergy < 0 & temperatureRecovery < maxTemperatureRecovery){
					valueOfRejection++;
					if (valueOfRejection >= maxIterationsfreezing / 1.5)
						temperature += (temperature * 0.2);          //reaquecimento
						f.write("Temperature recovery: " + temperature + "\n");
						temperatureRecovery++;
				}
				else
					valueOfRejection = 0;      //delta positivo (movimento aceito)

				if (deltaEnergy < 5)               //não foi um ganho significativo
					deterioration += 1;
				else
					deterioration = 0;

				if (deterioration >= maxIterations / temperature){          //não houve melhora significativa por max iterações						 
					temperature = coolingRate * temperature;									//Aplicar a taxa de resfriamento se a qualidade não tiver sofrido alterações significativas por 10 iterações
					f.write("Temperature : " + temperature + "\n");
					deterioration = 0;
					count = 0;
				}				

				f.close();
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
