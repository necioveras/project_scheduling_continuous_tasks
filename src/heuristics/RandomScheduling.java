package heuristics;

import model.Area;
import model.Member;
import model.Team;
import schedules.MemberMonthlySchedule;
import schedules.TeamMonthlySchedule;

public class RandomScheduling {
	
	public static TeamMonthlySchedule createRandomScheduling(Team team, Area areas[]){
		TeamMonthlySchedule tMonthSch = new TeamMonthlySchedule(team);
		
		for (Member m: team.getMembers()){
			MemberMonthlySchedule monthMember = new MemberMonthlySchedule(m);
			monthMember.randomAllocateTasks(areas);
			monthMember.updateIndexesMember();
			monthMember.calcQuality();
			tMonthSch.setMonthSchMember(m, monthMember);
		}
		tMonthSch.updateCalcsQuality(team, areas);
		return tMonthSch;
	}

}
