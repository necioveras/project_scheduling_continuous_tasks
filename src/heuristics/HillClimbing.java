package heuristics;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Random;

import model.Area;
import model.Member;
import model.Task;
import schedules.TeamMonthlySchedule;
import useful.Definitions;

public class HillClimbing {
	
	private static double flipMonth(Area areas[], int semana, int row, int col, TeamMonthlySchedule monthSchedule, 
			boolean acceptLowQuality, Member m, Task t, FileWriter f) throws IOException{
		double quality = monthSchedule.getQuality();
		//guarda tarefa anterior												
		Task taskPrior = monthSchedule.getTimeTable().get(m).getMonthly()[semana].getTasks()[row][col];
		//seta nova tarefa
		monthSchedule.getTimeTable().get(m).getMonthly()[semana].setTask(t, row, col);

		//atualiza os calculos para verificar se qualidade melhorou						
		monthSchedule.updateCalcsQuality(m, areas);

		if (acceptLowQuality == false)		
			if (monthSchedule.getQuality() < quality){  //qualidade PIOROU .... efetua-se um rollback
				if (taskPrior == null)
					monthSchedule.getTimeTable().get(m).getMonthly()[semana].setTaskNull(row, col);	
				else			
					monthSchedule.getTimeTable().get(m).getMonthly()[semana].setTask(taskPrior, row, col);

				monthSchedule.updateCalcsQuality(m, areas);                 //rollback também nos cálculos de qualidade
				return quality;    //valor inicial
			}
			else{  //qualidade melhorou
				f.write(monthSchedule.getQuality() + "\n");
				return monthSchedule.getQuality();				
			}
		else			
			return monthSchedule.getQuality();                    //Existirá uma probabilidade para esta solução ser aceita (Annealing Simulated)
	}


	public static void discoveryNextNeighborTeamSchedule(Area areas[], TeamMonthlySchedule monthSchedule, 
			boolean acceptLowQuality, FileWriter f) throws IOException{
		Random rCol = new Random(), rRow = new Random(),  rMember = new Random(), rMonthly = new Random();
		int row,col; 

		//seleciona uma memebro aleatório
		Member m = monthSchedule.getTeam().getMembers().get(rMember.nextInt((int)monthSchedule.getTeam().getSize()));
		//seleciona uma semana Aleatória
		int semana = rMonthly.nextInt(4);

		row = rRow.nextInt(Definitions.MAX_DAYS);
		col = rCol.nextInt(Definitions.MAX_INSTANT);

		//altera uma tarefa de uma semana
		for(Area a: areas){			
			for (Task t: a.getTasks()){				
				if(monthSchedule.getTimeTable().get(m).getMonthly()[semana].getTasks()[row][col] != null){
					if(!monthSchedule.getTimeTable().get(m).getMonthly()[semana].getTasks()[row][col].equals(t)){						
						flipMonth(areas, semana, row, col, monthSchedule, acceptLowQuality, m, t, f);																		
					}
				}
				else{
					flipMonth(areas, semana, row, col, monthSchedule, acceptLowQuality, m, t, f);
				}
			}
		}
	}


	public static void exec(TeamMonthlySchedule monthSchedule, Area areas[], int chancesValue){
		//Subida da colina              
		int chance = 0;
		FileWriter f;
		try {
			f = new FileWriter(new File ("qualidade.txt"));		
			do{
				double qualitySchedule = monthSchedule.getQuality();
				//atualiza Team Schedule atual para Team schedule vizinha	
				discoveryNextNeighborTeamSchedule(areas,monthSchedule, false, f);			
				if (monthSchedule.getQuality()> qualitySchedule)
					chance = 0;
				else 
					chance = chance+1;
			}while(chance<chancesValue);
			f.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}


}
