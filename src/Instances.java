import model.Member;
import model.Task;
import model.Team;
import schedules.MemberMonthlySchedule;
import schedules.MemberWeeklySchedule;
import schedules.TeamWeeklySchedule;


public class Instances {

	public static void main(String[] args) {
		
		Member necio = new Member(1, "Nécio");		
		MemberWeeklySchedule week1 = new MemberWeeklySchedule(necio); 		MemberWeeklySchedule week2 = new MemberWeeklySchedule(necio);
		MemberWeeklySchedule week3 = new MemberWeeklySchedule(necio); 		MemberWeeklySchedule week4 = new MemberWeeklySchedule(necio);
		
		Team team = new Team(1);
		team.addMember(necio);
		TeamWeeklySchedule teamWeek = new TeamWeeklySchedule(team);
		
		Task t1 = new Task(1,1,12, 17,2,0); Task t2 = new Task(2,1,7,13,4,1); Task t3 = new Task(3); Task t4 = new Task(4); Task t5 = new Task(5); Task t6 = new Task(6); Task t7 = new Task(7);
		Task t8 = new Task(8); Task t9 = new Task(9); Task t10 = new Task(10); Task t11 = new Task(11); Task t12 = new Task(12); Task t13 = new Task(13); Task t14 = new Task(14);
		Task t15 = new Task(15); Task t16 = new Task(16); Task t17 = new Task(17); Task t18 = new Task(18); Task t19 = new Task(19); Task t20 = new Task(20); Task t21 = new Task(21);				
		
		week1.setTask(t1, 1, 3);  week1.setTask(t1, 2, 3); week1.setTask(t1, 3, 3); week1.setTask(t1, 4, 3); week1.setTask(t1, 5, 8);
		week1.setTask(t1, 1, 6);  week1.setTask(t1, 2, 6); week1.setTask(t1, 3,6); week1.setTask(t1, 4, 6); week1.setTask(t1, 5,4);
		week1.setTask(t1, 1, 4);  week1.setTask(t1, 1, 8); week1.setTask(t1, 5,6); week1.setTask(t1, 5, 5); week1.setTask(t1, 5,2);
		
		week1.setTask(t2, 1, 1);  week1.setTask(t2, 2, 2); week1.setTask(t2, 3, 4); week1.setTask(t2, 4, 5); week1.setTask(t2, 5, 7);
		week1.setTask(t2, 1, 2);  week1.setTask(t2, 2, 8); week1.setTask(t2, 3,7); week1.setTask(t2, 4, 4); week1.setTask(t2, 5,1);		
		week1.setTask(t2, 1, 7);  week1.setTask(t2, 1, 8); 
		
		
		MemberMonthlySchedule monthSchedule = new MemberMonthlySchedule(necio);
		monthSchedule.addWeekShcedule(week1, 1); 		monthSchedule.addWeekShcedule(week2, 2);
		monthSchedule.addWeekShcedule(week3, 3);		monthSchedule.addWeekShcedule(week4, 4);
		
		teamWeek.addMemberWeeklySchedule(week1);
		teamWeek.getCumulateWorked(t1);
		teamWeek.getCumulateWorked(t2);
		
		week1.showTasks();
		System.out.println("Produtividade em t1 e t2: " + week1.getProductivity(t1) + "  |  " + week1.getProductivity(t2));
		System.out.println("Índices de T1 e T2 " + t1.calcIndex() + "  |  " + t2.calcIndex());
		monthSchedule.calcQuality();
		System.out.println("Qualidade da agenda mensal: " + monthSchedule.getQuality());
			
	}

}
