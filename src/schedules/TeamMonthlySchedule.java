package schedules;

import java.util.HashMap;
import java.util.Map;

import model.Area;
import model.Member;
import model.Task;
import model.Team;

public class TeamMonthlySchedule {

	private Team team;
	private double quality;
	private Map<Member, MemberMonthlySchedule> timeTable;
	
	/* ***************************************************************************************
	 *      						       								                      CONSTRUTORES  			 												                  * 				
	 * *************************************************************************************** */
	
	public TeamMonthlySchedule(Team t){
		team = t;
		timeTable = new HashMap<Member, MemberMonthlySchedule>();
		initialize();
	}
	
	/* ***************************************************************************************
	 *      						       									GETTERS           e          SETTERS  			 												         * 				
	 * *************************************************************************************** */

	public Team getTeam() {
		return team;
	}

	public void setTeam(Team team) {
		this.team = team;
	}

	public Map<Member, MemberMonthlySchedule> getTimeTable() {
		return timeTable;
	}

	public void setTimeTable(Map<Member, MemberMonthlySchedule> timeTable) {
		this.timeTable = timeTable;
	}

	public double getQuality() {
		return quality;
	}

	public void setQuality(double quality) {
		this.quality = quality;
	}
	
	/* ***************************************************************************************
	 *      						       									Métodos                       Privados  			 												         * 				
	 * *************************************************************************************** */
	
	private void initialize(){                                     //inicializar significa ZERAR a produtividade do time e o trabalho acumulado das tarefas
		for (Member m: team.getMembers()){
			m.getIndexes().clear(); //zerando os indices de produtividade dos membros
			timeTable.put(m, null);			
		}			
	}
	
	/* ***************************************************************************************
	 *      						       									Métodos                       Públicos  			 												         * 				
	 * *************************************************************************************** */		

	public void setMonthSchMember(Member m, MemberMonthlySchedule month){
		if (team.getMembers().contains(m))
			timeTable.put(m, month);
		else
			throw new IllegalArgumentException("Membro não faz parte do time!");
	}
	
	public void updateCumulateWorked(Task t){
		double sum = 0;		
		for (MemberMonthlySchedule month: timeTable.values())
				sum += month.getCumulateWorked(t);		
		t.setCumulateWorked(sum);
	}
	
	public void updateCumulateWorks(Area areas[]){
		for (Area a: areas)
			for (Task t: a.getTasks())
				updateCumulateWorked(t);
	}
	
	public void updateBulkWorked(Task t){
		double sum = 0;		
		for (MemberMonthlySchedule month: timeTable.values())
				sum += month.getBulkWorked(t);		
		t.setBulkWorked(sum);
	}
	
	public void updateBulkWorks(Area areas[]){
		for (Area a: areas)
			for (Task t: a.getTasks())
				updateBulkWorked(t);
	}
	
	public void updateIndexTask(Task t){
		double sum = 0;
		for (MemberMonthlySchedule month: timeTable.values())
			sum += month.getTaskIndex(t);
		t.setIndex(sum);
	}
	
	public void updateIndexesTasks(Area areas[]){
		for (Area a: areas)
			for (Task t: a.getTasks())
				updateIndexTask(t);
	}
	
	public void updateIndexesMember(){
		for (MemberMonthlySchedule month: timeTable.values())
			month.updateIndexesMember();
	}
	
	public double calcQuality(){
		double sum = 0;
		for (MemberMonthlySchedule month:  timeTable.values()){
			month.calcQuality();
			sum += month.getQuality();
		}
		setQuality(sum);
		return sum;
	}
	
	public void show(){
		for (MemberMonthlySchedule month:  timeTable.values()){
			System.out.println("Agenda mensal de "+ month.getMember().getName());
			month.show();
			System.out.println("Qualidade: " + month.getQuality());
		}
	}
	
	public void updateCalcsQuality(Member m, Area areas[]){
		getTimeTable().get(m).updateIndexesMember();
		getTimeTable().get(m).calcQuality();
		updateCumulateWorks(areas);
		updateIndexesTasks(areas);
		updateBulkWorks(areas);
		calcQuality();
	}
	
	public void updateCalcsQuality(Team t, Area areas[]){
		for (Member m: team.getMembers())
			updateCalcsQuality(m, areas);
	}
	
	/* ***************************************************************************************
	 *      						       ------------------------------------------------------------------------------------------										         * 				
	 * *************************************************************************************** */


}
