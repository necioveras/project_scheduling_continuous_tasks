package schedules;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import model.Area;
import model.Member;
import model.Task;
import useful.Definitions;


public class MemberMonthlySchedule {
	
	private MemberWeeklySchedule[] monthly = new MemberWeeklySchedule[4];
	private Member member;
	private int count;                                                                                 															   //Contagem dos índices não repetidos da agenda
	private Map <Task, Double> uniqueIndexes = new HashMap<Task, Double>();           //Conjunto dos índices não repetidos da agenda
	private double quality = 0;
	
	/* ***************************************************************************************
	 *      						       								                      CONSTRUTORES  			 												                  * 				
	 * *************************************************************************************** */
	
	public MemberMonthlySchedule(Member m){
		member = m;
		quality = 0;
		initialize();
	}
	
	
	/* ***************************************************************************************
	 *      						       									GETTERS           e          SETTERS  			 												         * 				
	 * *************************************************************************************** */
	
	public Member getMember() {
		return member;
	}


	public void setMember(Member member) {
		this.member = member;
	}


	public MemberWeeklySchedule[] getMonthly() {
		return monthly;
	}


	public int getCount() {
		return count;
	}


	public Map <Task, Double> getUniqueIndexes() {
		return uniqueIndexes;
	}


	public double getQuality() {
		return quality;
	}


	
	
	/* ***************************************************************************************
	 *      						       									Métodos                       Privados  			 												         * 				
	 * *************************************************************************************** */
	
	private void initialize(){
		for (int i=0; i < monthly.length; i++)
			monthly[i] = new MemberWeeklySchedule(member);
	}
	
	private void generateUniqueIndexes(){
		uniqueIndexes.clear();
		count = 0;
		for (int i=0; i < monthly.length; i++){
			//para cada semana....
			Task[][] t = monthly[i].getTasks();
			for (int d=0; d < monthly[i].getNUMBER_OF_DAYS(); d++)
				for (int k=0; k < monthly[i].getNUMBER_OF_INSTANTS(); k++)
					if (t[d][k] != null)
						uniqueIndexes.put(t[d][k], 0.0);
		}					
		count = uniqueIndexes.size();
	}
	
	
	
	/* ***************************************************************************************
	 *      						       									Métodos                       Públicos  			 												         * 				
	 * *************************************************************************************** */

	public void updateIndexesMember(){
		generateUniqueIndexes();
		Object[] o = (Object[]) uniqueIndexes.keySet().toArray();
		for (int i =0; i < count; i++){
			Task t = (Task) o[i];
			double productivity = 0;
			for (int j=0; j < monthly.length; j++)
				productivity += monthly[j].getProductivity(t);
			member.calcIndex(t, productivity);
		}
	}	
	
	public double getCumulateWorked(Task t){		
		double sum = 0;
		for (int j=0; j < monthly.length; j++)
			sum += monthly[j].getProductivity(t);
		return sum;
	}
	
	public double getBulkWorked(Task t){		
		double sum = 0;
		for (int j=0; j < monthly.length; j++)
			sum += monthly[j].getBulkWorks(t);
		return sum;
	}
	
	public double getTaskIndex(Task t){
		generateUniqueIndexes();
		return (getCumulateWorked(t) / t.getEstimedBulk());
	}
	
	public void calcQuality(){
		generateUniqueIndexes();		
		quality = 0;
		Object[] o = (Object[]) uniqueIndexes.keySet().toArray();
		for (int i =0; i < count; i++){																							//Somatório de bonificação
			if (o[i] != null){
				Task t = (Task) o[i];
				quality += (member.getIndex(t) / t.getGoal()) * (t.getUtility() / t.getCost()); 
			}			
		}

		double penaltyA = 0, penaltyB = 0, penaltyC = 0;
		for (Task t: member.getAvailableTasks()){																				//Temos a lista de tarefas associadas ao membro
			if (t.getIndex() > t.getGoal())
				penaltyA += (t.getIndex() - t.getGoal()) / (t.getUtility()/t.getCost());            //Somatório de punição (Índices acima da meta)
			else if (t.getIndex() < t.getGoal())
				penaltyB += (t.getGoal() - t.getIndex()) * (t.getUtility()/t.getCost());				//Somatório de punição (Índices abaixo da meta)
			if (t.getIndex() == 0 || t.getIndex() < 1.0)                                                                                                                     //Tarefa NÃO trabalhada
				penaltyC += t.getEstimedBulk() * (t.getUtility() / t.getCost());
		}
				
		
		quality = quality - penaltyA - penaltyB - penaltyC;			
																																				
	}
	
	public double getProductivity(Task t){
		double sum = 0;
		for (int j=0; j < monthly.length; j++)
			sum += monthly[j].getProductivity(t);
		return sum;
	}
	
	public void addWeekSchedule(MemberWeeklySchedule week, int seq){
		if (seq-1 <= 4 && seq-1 >= 0)
			monthly[seq-1] = week;
		else
			throw new IllegalArgumentException("Parâmetro 'seq' inválido para um sequência de semana");
	}
	
	public void show(){		
		for (int j=0; j < monthly.length; j++){
			System.out.println("Semana " + (j+1));
			monthly[j].showTasks();
		}
	}


	public void randomAllocateTasks(Area[] areas) {   //Aloca as tarefas aleatoriamente na agenda
		Random rCol = new Random(), rRow = new Random(),  rBool = new Random(), rTimes = new Random();
		for (Area a : areas)
			for (Task t: a.getTasks()){						//Todas as tarefas de todas as áreas
				for (MemberWeeklySchedule week: monthly) {                  //Para todas as semanas
					if (rBool.nextInt(10) < 5){               //a tarefa está selecionada para entrar na agenda (50% de chance de ser selecionada)
						int nTimes = rTimes.nextInt((int)t.getDelta()+1);            //numero de vezes ela entrará em casa semana
						for (int i=0; i < nTimes; i++)                                                          //coloca efetivamente a tarefa nVezes na agenda da SEMANA
							if (getMember().getAvailableTasks().contains(t))
								week.setTask(t, rCol.nextInt(Definitions.MAX_DAYS), rRow.nextInt(Definitions.MAX_INSTANT));							
					}
				}					
			}
	}
	
	public void randomAllocateTasks2(Area[] areas) {   //Aloca as tarefas aleatoriamente na agenda
		Random rCol = new Random(), rRow = new Random(),  rBool = new Random(), rTimes = new Random();
		for (Area a : areas){
			for (Task t: a.getTasks()){	//Todas as tarefas de todas as áreas	
				for (MemberWeeklySchedule week: monthly) {                  //Para todas as semanas
					if (rBool.nextInt(10) < 5){               //a tarefa está selecionada para entrar na agenda (50% de chance de ser selecionada)
						int nTimes = rTimes.nextInt((int)t.getDelta());            //numero de vezes ela entrará em cada semana
						for (int i=0; i < nTimes; i++)        {                                                  //coloca efetivamente a tarefa nVezes na agenda da SEMANA
							//			week.setTask(t, rCol.nextInt(Definitions.MAX_DAYS), rRow.nextInt(Definitions.MAX_INSTANT));
							int Row = rRow.nextInt(Definitions.MAX_DAYS);
							int Col = rCol.nextInt(Definitions.MAX_INSTANT);
							while(week.getTasks()[Row][Col] != null){
								Row = rRow.nextInt(Definitions.MAX_DAYS);
								Col = rCol.nextInt(Definitions.MAX_INSTANT);
							}
							week.setTask(t, Row, Col);
						}       
					} 
				}	
			}
		}
	}
	
	/* ***************************************************************************************
	 *      						       ------------------------------------------------------------------------------------------										         * 				
	 * *************************************************************************************** */
}
