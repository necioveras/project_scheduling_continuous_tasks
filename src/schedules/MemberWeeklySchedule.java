package schedules;

import useful.Definitions;
import model.Member;
import model.Task;

public class MemberWeeklySchedule {
	
	private int NUMBER_OF_DAYS = 5;                       //referente aos dias da semana
	private int NUMBER_OF_INSTANTS = 8;           //referente aos instantes para um dia de trabalho
	
	private Member member;
	private Task[][] tasks; 
	
	
	/* ***************************************************************************************
	 *      						       								                      CONSTRUTORES  			 												                  * 				
	 * *************************************************************************************** */
	public MemberWeeklySchedule(Member m){
		member = m;
		tasks = new Task[NUMBER_OF_DAYS][NUMBER_OF_INSTANTS];		
	}
	
	/* ***************************************************************************************
	 *      						       									GETTERS           e          SETTERS  			 												         * 				
	 * *************************************************************************************** */

	public int getNUMBER_OF_DAYS() {
		return NUMBER_OF_DAYS;
	}

	public void setNUMBER_OF_DAYS(int nUMBER_OF_DAYS) {
		if (nUMBER_OF_DAYS <= Definitions.MAX_DAYS)
				NUMBER_OF_DAYS = nUMBER_OF_DAYS;
	}

	public int getNUMBER_OF_INSTANTS() {
		return NUMBER_OF_INSTANTS;
	}

	public void setNUMBER_OF_INSTANTS(int nUMBER_OF_INSTANTS) {
		if (nUMBER_OF_INSTANTS <= Definitions.MAX_INSTANT)
				NUMBER_OF_INSTANTS = nUMBER_OF_INSTANTS;
	}	
	
	public Member getMember() {
		return member;
	}

	public void setMember(Member member) {
		this.member = member;
	}
	
	public Task[][] getTasks() {
		return tasks;
	}

	public void setTasks(Task[][] tasks) {
		this.tasks = tasks;
	}	
	
	
	/* ***************************************************************************************
	 *      						       									Métodos públicos   			 												                                  * 				
	 * *************************************************************************************** */	

	public void setTask(Task t, int day, int instant){
		if (day <0 || day > NUMBER_OF_DAYS || instant < 0 || instant > NUMBER_OF_INSTANTS)
			throw new IllegalArgumentException("Parâmetro inválido para um agendamento de tarefa.");
                else{
                    if (member.getAvailableTasks().contains(t)) //Verifica se o Membro pode executar a tarefa
                    	tasks[day][instant] = t;
                }
	}
	
	public void setTaskNull( int day, int instant){
		if (day <0 || day > NUMBER_OF_DAYS || instant < 0 || instant > NUMBER_OF_INSTANTS)
			throw new IllegalArgumentException("Parâmetro inválido para um agendamento de tarefa.");
                else                    
                    	tasks[day][instant] = null;
	}
	
	public Task getActivity (int day, int instant){                //retorna a tarefa realizada pelo desenvolvedor no dia d e instante i
		return tasks[day][instant];
	}
	
	public double getProductivity (Task t){									//retorna a produtividade do desenvolvedor para uma tarefa t
		double sum = 0.0;
		for (int d=0; d < NUMBER_OF_DAYS; d++)
			for (int k=0; k < NUMBER_OF_INSTANTS; k++)
				if (tasks[d][k] != null )
					if (tasks[d][k].equals(t))
						sum += tasks[d][k].getCost();		
		return sum;
	}		
	
	public double getBulkWorks(Task t){									//retorna o volume de trabalho do desenvolvedor para uma tarefa
		double sum = 0.0;
		for (int d=0; d < NUMBER_OF_DAYS; d++)
			for (int k=0; k < NUMBER_OF_INSTANTS; k++)
				if (tasks[d][k] != null )
					if (tasks[d][k].equals(t))
						sum += 1.0;		
		return sum;
	}
	
	public void showTasks(){
		for (int d=0; d < NUMBER_OF_DAYS; d++){
			for (int k=0; k < NUMBER_OF_INSTANTS; k++)
				if (tasks[d][k] != null)
					System.out.print(tasks[d][k].getId() + "  ");
				else
					System.out.print(0 + "  ");
			System.out.println();
		}
	}
	
	/* ***************************************************************************************
	 *      						       ------------------------------------------------------------------------------------------										         * 				
	 * *************************************************************************************** */

}
