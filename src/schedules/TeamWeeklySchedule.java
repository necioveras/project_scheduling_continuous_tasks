package schedules;

import java.util.LinkedList;
import java.util.List;

import model.Member;
import model.Task;
import model.Team;

public class TeamWeeklySchedule {
	
	Team team;
	List <MemberWeeklySchedule> timeTable;
	
	/* ***************************************************************************************
	 *      						       								                      CONSTRUTORES  			 												                  * 				
	 * *************************************************************************************** */
	public TeamWeeklySchedule(Team t){
		this.team = t;
		timeTable = new LinkedList<MemberWeeklySchedule>();
		initialize();
	}
	
	/* ***************************************************************************************
	 *      						       									GETTERS           e          SETTERS  			 												         * 				
	 * *************************************************************************************** */
	
	public Team getTeam() {
		return team;
	}

	public void setTeam(Team team) {
		this.team = team;
	}

	public List<MemberWeeklySchedule> getTimeTable() {
		return timeTable;
	}

	public void setTimeTable(List<MemberWeeklySchedule> timeTable) {
		this.timeTable = timeTable;
	}
	
	public void addMemberWeeklySchedule (MemberWeeklySchedule week){
		timeTable.add(week);
	}
	
	/* ***************************************************************************************
	 *      						       									Métodos                       Privados  			 												         * 				
	 * *************************************************************************************** */
	
	private void initialize(){
		for (Member m: team.getMembers()){
			timeTable.add(new MemberWeeklySchedule(m));
		}
	}
	
	/* ***************************************************************************************
	 *      						       									Métodos                       Públicos  			 												         * 				
	 * *************************************************************************************** */
	
	public int getCumulateWorked(Task t){														//Retorna o trabalho acumulado de uma determinada tarefa 
		int sum = 0;
		for (MemberWeeklySchedule mws: timeTable)
			sum += mws.getProductivity(t);
		t.setCumulateWorked(sum);
		return sum;
	}
	
	/* ***************************************************************************************
	 *      						       ------------------------------------------------------------------------------------------										         * 				
	 * *************************************************************************************** */
}
