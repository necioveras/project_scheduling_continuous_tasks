package model;

import java.util.LinkedList;
import java.util.List;

public class Area {
	
	private int size;
	List<Task> tasks;
	
	/* ***************************************************************************************
	 *      						       								                      CONSTRUTORES  			 												                  * 				
	 * *************************************************************************************** */
	
	public Area(){
		this(0);
	}
	
	public Area(int size){
		this.size = size;
		tasks = new LinkedList<Task>();
		initialize();
	}
	
	/* ***************************************************************************************
	 *      						       									GETTERS           e          SETTERS  			 												         * 				
	 * *************************************************************************************** */
	
	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public List<Task> getTasks() {
		return tasks;
	}

	public void setTasks(List<Task> tasks) {
		this.tasks = tasks;
	}
	
	/* ***************************************************************************************
	 *      						       									Métodos                  Públicos											                                  * 				
	 * *************************************************************************************** */

	public int totalEstimedBulk(){   //retorna o volume total de trabalho estimado da área
		int sum = 0;
		for (Task t: tasks)
			sum += t.getEstimedBulk();
		return sum;
	}
	
	public void addTask(Task t){
		tasks.add(t);
		size += 1;
	}
	
	public void evolutionGoals(double percent){
		for (Task t: tasks)
			t.setGoal(t.getGoal() + (t.getGoal() * (percent/100)));
	}
	
	
	/* ***************************************************************************************
	 *      						       									Métodos                  Privados  			 												              * 				
	 * *************************************************************************************** */
	
	private void initialize(){
		tasks.clear();
		for (int i = 0; i < size; i++)
			tasks.add(new Task());
	}
	
	/* ***************************************************************************************
	 *      						       ------------------------------------------------------------------------------------------										         * 				
	 * *************************************************************************************** */	

}
