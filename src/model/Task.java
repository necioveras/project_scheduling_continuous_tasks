package model;

import useful.Definitions;

public class Task implements Comparable<Task> {
	
	private int id;
	private double cost;							        	//custo
	private double estimedBulk;                   //volume estimado
	private double goal;							  //meta
	private int utility;                                 //importância
	private double index;                         //índice
	private double delta;                                   //estimativa de repetições semanais para o cumprimento da tarefa
	private double cumulateWorked;       //trabalho acumulado
	private double bulkWorked;                  //volume de trabalho
	
	/* ***************************************************************************************
	 *      						       								                      CONSTRUTORES  			 												                  * 				
	 * *************************************************************************************** */
	public Task(){
		
	}	
	
	public Task(int id){
		//valores ilustrativos (para testes)
		this.id = id;
		cost = 1;
		estimedBulk = cost;		
		setDelta(15);
		calcEstimedBulk();
		index = 0;
	}
	
	public Task(int id, double cost, int delta,  int utility){
		this.id = id;
		this.cost = cost;
		setDelta(delta);
		calcEstimedBulk();
		this.utility = utility;
	}
	
	public Task(int id, double cost, int delta,  double goal, int utility, double index){
		this.id = id;
		this.cost = cost;
		setDelta(delta);
		calcEstimedBulk();
		this.goal = goal;
		this.utility = utility;
		this.index = index;
	}
	
	/* ***************************************************************************************
	 *      						       									GETTERS           e          SETTERS  			 												         * 				
	 * *************************************************************************************** */
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public double getCost() {
		return cost;
	}
	public void setCost(double cost) {
		this.cost = cost;
		calcEstimedBulk();
	}
	public double getEstimedBulk() {
		return estimedBulk;
	}	
	public double getGoal() {
		return goal;
	}
	public void setGoal(double goal) {
		this.goal = goal;
	}
	public int getUtility() {
		return utility;
	}
	public void setUtility(int utility) {
		this.utility = utility;
	}
	public double getIndex() {
		return index;
	}
	public void setIndex(double index) {
		this.index = index;
	}
	public double getDelta() {
		return delta;
	}

	public void setDelta(double delta) {
		if (delta <= (Definitions.MAX_DAYS * Definitions.MAX_INSTANT))
				this.delta = delta;
		else throw new IllegalArgumentException("Valor do delta acima do limite de possibilidades para uma agenda semanal");
		calcEstimedBulk();
	}
	
	public double getCumulateWorked() {
		return cumulateWorked;
	}
	
	public void setCumulateWorked(double cumulateWorked) {
		this.cumulateWorked = cumulateWorked;
	}
	
	public double getBulkWorked() {
		return bulkWorked;
	}
	
	public void setBulkWorked(double bulkWorked){
		this.bulkWorked = bulkWorked;
	}
	
	/* ***************************************************************************************
	 *      						       									Métodos                  Públicos											                                  * 				
	 * *************************************************************************************** */				

	public void calcEstimedBulk(){
		estimedBulk = cost * delta;
	}	
	
	public boolean equals(Object o){
		Task t = (Task) o;
		return (t.getId() == id);
	}

	public int compareTo(Task o) {
		if (o.getId() == id)
			return 0;
		else
			return 1;
	}
	
	
	
	/* ***************************************************************************************
	 *      						       ------------------------------------------------------------------------------------------										         * 				
	 * *************************************************************************************** */
	

}
