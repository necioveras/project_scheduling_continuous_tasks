package model;

import java.util.LinkedList;
import java.util.List;

public class Team {
	
	private int size;
	List<Member> members;
	
	/* ***************************************************************************************
	 *      						       								                      CONSTRUTORES  			 												                  * 				
	 * *************************************************************************************** */
	public Team(){
		size = 0;
		members = new LinkedList<Member>();
	}
	
	public Team(int size){
		this.size = size;
		members = new LinkedList<Member>();
		initialize();
	}
	
	/* ***************************************************************************************
	 *      						       									GETTERS           e          SETTERS  			 												         * 				
	 * *************************************************************************************** */

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public List<Member> getMembers() {
		return members;
	}

	public void setMembers(List<Member> members) {
		this.members = members;
	}
	
	public void addMember(Member m){
		members.add(m);
		size += 1;
	}
	
	
	/* ***************************************************************************************
	 *      						       									Métodos                  Privados  			 												              * 				
	 * *************************************************************************************** */
	
	private void initialize(){
		members.clear();
		for (int i = 0; i < size; i++)
			members.add(new Member());			
	}
	
	/* ***************************************************************************************
	 *      						       ------------------------------------------------------------------------------------------										         * 				
	 * *************************************************************************************** */	


}
