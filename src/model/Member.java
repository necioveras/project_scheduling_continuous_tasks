package model;

import java.util.HashMap;
import java.util.Map;
import java.util.List;
import java.util.LinkedList;

public class Member {

	private int id; 
	private String name;
	private Map<Task, Double> indexes; 
	private List<Task> availableTasks; 

	/* ***************************************************************************************
	 *      						       								                      CONSTRUTORES  			 												                  * 				
	 * *************************************************************************************** */

	public Member(){
		indexes = new HashMap<Task, Double>();
		availableTasks = new LinkedList<Task>();		
	}

	public Member(int id, String name){
		this.id = id;
		this.name = name;
		indexes = new HashMap<Task, Double>();
		availableTasks = new LinkedList<Task>();
	}

	/* ***************************************************************************************
	 *      						       									GETTERS           e          SETTERS  			 												         * 				
	 * *************************************************************************************** */

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public Map<Task, Double> getIndexes() {
		return indexes;
	}

	public void mapTask(Task t, double index){
		indexes.put(t, index);
	}

	public double getIndex(Task t){
		return indexes.containsKey(t) ?  indexes.get(t) : 0;
	}

	public List<Task> getAvailableTasks() {
		return availableTasks;
	}

	public void setAvailableTasks(List<Task> availableTasks) {
		this.availableTasks = availableTasks;
	}
	
	public void setAvailableTask(Task t){
		availableTasks.add(t);
	}
	
	public void setAvailableTasks(Task t[]){
		for (int i = 0; i < t.length; i++)
			availableTasks.add(t[i]);
	}

	/* ***************************************************************************************
	 *      						       									Métodos                  Públicos											                                  * 				
	* *************************************************************************************** */					

	public void calcIndex(Task t, double productivity){		
		double index = (productivity / t.getEstimedBulk());								
		indexes.put(t, index);
	}
	
	public boolean equals(Object o){
		Member m = (Member) o;
		return m.getId() == getId();
	}

	/* ***************************************************************************************
	 *      						       ------------------------------------------------------------------------------------------										         * 				
	 * *************************************************************************************** */

}
