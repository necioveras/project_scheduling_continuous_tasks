package tests;

import model.Area;
import model.Member;
import model.Task;
import model.Team;
import schedules.MemberMonthlySchedule;
import schedules.TeamMonthlySchedule;

public class SimpleScheduling {
	
	public static TeamMonthlySchedule createRandomScheduling(Team team, Area areas[]){
		TeamMonthlySchedule tMonthSch = new TeamMonthlySchedule(team);
		
		for (Member m: team.getMembers()){
			MemberMonthlySchedule monthMember = new MemberMonthlySchedule(m);
			monthMember.randomAllocateTasks(areas);
			monthMember.updateIndexesMember();
			monthMember.calcQuality();
			tMonthSch.setMonthSchMember(m, monthMember);
		}
		tMonthSch.updateCumulateWorks(areas);
		tMonthSch.updateIndexesTasks(areas);
		tMonthSch.calcQuality();
		return tMonthSch;
	}
	

	public static void main(String[] args) {
		Team team = new Team();
		Member m1 = new Member(1, "Nécio");
		Member m2= new Member(2, "Mateus");		
		team.addMember(m1); team.addMember(m2);
		
		Area areas[] = new Area[2];
		areas[0] = new Area(); areas[1] = new Area();
		Task t1 = new Task(1, 1, 7, 16, 1, 0);
		Task t2= new Task(2, 1, 7, 16, 1, 0);
		Task t3 = new Task(3, 1, 7, 16, 1, 0);
		Task t4 = new Task(4, 1, 7, 16, 1, 0);
		areas[0].addTask(t1); areas[0].addTask(t2);
		areas[1].addTask(t3); areas[1].addTask(t4);
		
		//Mês 1
		
		TeamMonthlySchedule monthSchedule = createRandomScheduling(team, areas);
		monthSchedule.show();
		System.out.println();		
		
		System.out.println("Dados para o índice T1: Trab.Acum:" + t1.getCumulateWorked()+ " Índice: " + t1.getIndex());
		System.out.println("Dados para o índice T2: Trab.Acum:" + t2.getCumulateWorked()+ " Índice: " + t2.getIndex());
		System.out.println("Dados para o índice T3: Trab.Acum:" + t3.getCumulateWorked()+ " Índice: " + t3.getIndex());
		System.out.println("Dados para o índice T4: Trab.Acum:" + t4.getCumulateWorked()+ " Índice: " + t4.getIndex());
		
		System.out.println();
		System.out.println("Volume estimado de T1: " + t1.getEstimedBulk());
		System.out.println("Membro " + m1.getName() + ": Produtividade T1: " + monthSchedule.getTimeTable().get(m1).getProductivity(t1));		
		System.out.println("Membro " + m1.getName() + ": Índice em T1: " + monthSchedule.getTimeTable().get(m1).getMember().getIndex(t1));
		System.out.println("Membro " + m2.getName() + ": Produtividade T1: " + monthSchedule.getTimeTable().get(m2).getProductivity(t1));
		System.out.println("Membro " + m2.getName() + ": Índice em T1: " + monthSchedule.getTimeTable().get(m2).getMember().getIndex(t1));
		
		System.out.println("\n ... \n");
		System.out.println("Volume estimado de T4: " + t4.getEstimedBulk());
		System.out.println("Membro " + m1.getName() + ": Produtividade t4: " + monthSchedule.getTimeTable().get(m1).getProductivity(t4));		
		System.out.println("Membro " + m1.getName() + ": Índice em t4: " + monthSchedule.getTimeTable().get(m1).getMember().getIndex(t4));
		System.out.println("Membro " + m2.getName() + ": Produtividade t4: " + monthSchedule.getTimeTable().get(m2).getProductivity(t4));
		System.out.println("Membro " + m2.getName() + ": Índice em t4: " + monthSchedule.getTimeTable().get(m2).getMember().getIndex(t4));
		
		System.out.println();		
		System.out.println("Qualidade da agenda mensal de " +m1.getName() + " : " + monthSchedule.getTimeTable().get(m1).getQuality());
		System.out.println("Qualidade da agenda mensal de " +m2.getName() + " : " + monthSchedule.getTimeTable().get(m2).getQuality());
		System.out.println("Qualidade da agenda mensal da equipe: " + monthSchedule.getQuality());
		
		
		//Mês 2
		System.out.println("\n ------------------------------ Mês 2 ------------------------------------\n");
		
		TeamMonthlySchedule monthSchedule2 = createRandomScheduling(team, areas);
		monthSchedule2.show();
		System.out.println();		
		
		System.out.println("Dados para o índice T1: Trab.Acum:" + t1.getCumulateWorked()+ " Índice: " + t1.getIndex());
		System.out.println("Dados para o índice T2: Trab.Acum:" + t2.getCumulateWorked()+ " Índice: " + t2.getIndex());
		System.out.println("Dados para o índice T3: Trab.Acum:" + t3.getCumulateWorked()+ " Índice: " + t3.getIndex());
		System.out.println("Dados para o índice T4: Trab.Acum:" + t4.getCumulateWorked()+ " Índice: " + t4.getIndex());
		
		System.out.println();
		System.out.println("Volume estimado de T1: " + t1.getEstimedBulk());
		System.out.println("Membro " + m1.getName() + ": Produtividade T1: " + monthSchedule2.getTimeTable().get(m1).getProductivity(t1));		
		System.out.println("Membro " + m1.getName() + ": Índice em T1: " + monthSchedule2.getTimeTable().get(m1).getMember().getIndex(t1));
		System.out.println("Membro " + m2.getName() + ": Produtividade T1: " + monthSchedule2.getTimeTable().get(m2).getProductivity(t1));
		System.out.println("Membro " + m2.getName() + ": Índice em T1: " + monthSchedule2.getTimeTable().get(m2).getMember().getIndex(t1));
		
		System.out.println("\n ... \n");
		System.out.println("Volume estimado de T4: " + t4.getEstimedBulk());
		System.out.println("Membro " + m1.getName() + ": Produtividade t4: " + monthSchedule2.getTimeTable().get(m1).getProductivity(t4));		
		System.out.println("Membro " + m1.getName() + ": Índice em t4: " + monthSchedule2.getTimeTable().get(m1).getMember().getIndex(t4));
		System.out.println("Membro " + m2.getName() + ": Produtividade t4: " + monthSchedule2.getTimeTable().get(m2).getProductivity(t4));
		System.out.println("Membro " + m2.getName() + ": Índice em t4: " + monthSchedule2.getTimeTable().get(m2).getMember().getIndex(t4));
		
		System.out.println();
		System.out.println("Qualidade da agenda mensal de " +m1.getName() + " : " + monthSchedule2.getTimeTable().get(m1).getQuality());
		System.out.println("Qualidade da agenda mensalde " +m2.getName() + " : " + monthSchedule2.getTimeTable().get(m2).getQuality());
		System.out.println("Qualidade da agenda mensal da equipe: " + monthSchedule2.getQuality());
		
	}

}
