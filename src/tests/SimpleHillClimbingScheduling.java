package tests;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Random;

import model.Area;
import model.Member;
import model.Task;
import model.Team;
import schedules.TeamMonthlySchedule;
import useful.Definitions;

public class SimpleHillClimbingScheduling {
	
	private static double flipMonth(Area areas[], int semana, int row, int col, TeamMonthlySchedule monthSchedule, Member m, Task t, FileWriter f) throws IOException{
			double quality = monthSchedule.getQuality();
			//guarda tarefa anterior												
			Task taskAn = monthSchedule.getTimeTable().get(m).getMonthly()[semana].getTasks()[row][col];
			//seta nova tarefa
			monthSchedule.getTimeTable().get(m).getMonthly()[semana].setTask(t, row, col);
	
			//atualiza os calculos para verificar se qualidade melhorou						
			monthSchedule.updateCalcsQuality(m, areas);
			
			if (monthSchedule.getQuality() < quality){  //qualidade PIOROU .... efetua-se um rollback
				monthSchedule.getTimeTable().get(m).getMonthly()[semana].setTask(taskAn, row, col);
				monthSchedule.updateCalcsQuality(m, areas);                 //rollback também nos cálculos de qualidade
				return quality;    //valor inicial
			}
			else{  //qualidade melhorou
				f.write(monthSchedule.getQuality() + "\n");
				return monthSchedule.getQuality();				
			}						
	}

	public static void DiscoveryNextNeighborTeamSchedule(Area areas[], TeamMonthlySchedule monthSchedule, FileWriter f) throws IOException{
		Random rCol = new Random(), rRow = new Random(),  rMember = new Random(), rMonthly = new Random();
		int row,col; 

		//seleciona uma memebro aleatório
		Member m = monthSchedule.getTeam().getMembers().get(rMember.nextInt((int)monthSchedule.getTeam().getSize()));
		//seleciona uma semana Aleatória
		int semana = rMonthly.nextInt(4);

		row = rRow.nextInt(Definitions.MAX_DAYS);
		col = rCol.nextInt(Definitions.MAX_INSTANT);
				
		//altera uma tarefa de uma semana
		for(Area a: areas){			
			for (Task t: a.getTasks()){				
				if(monthSchedule.getTimeTable().get(m).getMonthly()[semana].getTasks()[row][col] != null){
					if(!monthSchedule.getTimeTable().get(m).getMonthly()[semana].getTasks()[row][col].equals(t)){						
						 flipMonth(areas, semana, row, col, monthSchedule, m, t, f);																		
					}
				}
				else{
					flipMonth(areas, semana, row, col, monthSchedule, m, t, f);
				}
			}
		}
	}


	public static void main(String[] args) {
		Team team = new Team();
		Member m1 = new Member(1, "Nécio");
		Member m2= new Member(2, "Mateus");		
		team.addMember(m1); team.addMember(m2);
		team.setSize(2);

		Area areas[] = new Area[2];
		areas[0] = new Area(); areas[1] = new Area();
		Task t1 = new Task(1, 2, 12, 10, 7, 0);
		Task t2= new Task(2, 3, 10, 8, 6, 0);
		Task t3 = new Task(3, 5, 6, 5, 9, 0);
		Task t4 = new Task(4, 9, 15, 10, 16, 0);
		areas[0].addTask(t1); areas[0].addTask(t2);
		areas[1].addTask(t3); areas[1].addTask(t4);
		m1.setAvailableTask(t1); m1.setAvailableTask(t2); m1.setAvailableTask(t3); m1.setAvailableTask(t4);
		m2.setAvailableTask(t1); m2.setAvailableTask(t2);	m2.setAvailableTask(t3); m2.setAvailableTask(t4);

		//Mês 1

		///*
		TeamMonthlySchedule monthSchedule = new TeamMonthlySchedule(team);
		monthSchedule = SimpleRandomScheduling.createRandomScheduling(team, areas);
		System.out.println("Pop. inicial:");
		monthSchedule.show();
		//List<MemberMonthlySchedule> necioSchedules = new LinkedList<MemberMonthlySchedule>();


		//Subida da colina              
		int chances = 0;
		FileWriter f;
		try {
			f = new FileWriter(new File ("qualidade.txt"));		
			do{
				double qualitySchedule = monthSchedule.getQuality();
				//atualiza Team Schedule atual para Team schedule vizinha	
				DiscoveryNextNeighborTeamSchedule(areas,monthSchedule, f);			
				if (monthSchedule.getQuality()> qualitySchedule)
					chances = 0;
				else 
					chances = chances+1;
			}while(chances<1000);
			f.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		//*/



		monthSchedule.show();
		System.out.println();		

		System.out.println("Dados para o índice T1: Vol. Trab.:" + t1.getBulkWorked()+ " Índice: " + t1.getIndex());
		System.out.println("Dados para o índice T2: Vol. Trab.:" + t2.getBulkWorked()+ " Índice: " + t2.getIndex());
		System.out.println("Dados para o índice T3: Vol. Trab.:" + t3.getBulkWorked()+ " Índice: " + t3.getIndex());
		System.out.println("Dados para o índice T4: Vol. Trab.:" + t4.getBulkWorked()+ " Índice: " + t4.getIndex());

		System.out.println();
		System.out.println("Volume estimado de T1: " + t1.getEstimedBulk());
		System.out.println("Membro " + m1.getName() + ": Produtividade T1: " + monthSchedule.getTimeTable().get(m1).getProductivity(t1));		
		System.out.println("Membro " + m1.getName() + ": Índice em T1: " + monthSchedule.getTimeTable().get(m1).getMember().getIndex(t1));
		System.out.println("Membro " + m2.getName() + ": Produtividade T1: " + monthSchedule.getTimeTable().get(m2).getProductivity(t1));
		System.out.println("Membro " + m2.getName() + ": Índice em T1: " + monthSchedule.getTimeTable().get(m2).getMember().getIndex(t1));
		
		System.out.println();
		System.out.println("Volume estimado de t2: " + t2.getEstimedBulk());
		System.out.println("Membro " + m1.getName() + ": Produtividade t2: " + monthSchedule.getTimeTable().get(m1).getProductivity(t2));		
		System.out.println("Membro " + m1.getName() + ": Índice em t2: " + monthSchedule.getTimeTable().get(m1).getMember().getIndex(t2));
		System.out.println("Membro " + m2.getName() + ": Produtividade t2: " + monthSchedule.getTimeTable().get(m2).getProductivity(t2));
		System.out.println("Membro " + m2.getName() + ": Índice em t2: " + monthSchedule.getTimeTable().get(m2).getMember().getIndex(t2));

		System.out.println();
		System.out.println("Volume estimado de t3: " + t3.getEstimedBulk());
		System.out.println("Membro " + m1.getName() + ": Produtividade t3: " + monthSchedule.getTimeTable().get(m1).getProductivity(t3));		
		System.out.println("Membro " + m1.getName() + ": Índice em t3: " + monthSchedule.getTimeTable().get(m1).getMember().getIndex(t3));
		System.out.println("Membro " + m2.getName() + ": Produtividade t3: " + monthSchedule.getTimeTable().get(m2).getProductivity(t3));
		System.out.println("Membro " + m2.getName() + ": Índice em t3: " + monthSchedule.getTimeTable().get(m2).getMember().getIndex(t3));

		System.out.println();
		System.out.println("Volume estimado de T4: " + t4.getEstimedBulk());
		System.out.println("Membro " + m1.getName() + ": Produtividade t4: " + monthSchedule.getTimeTable().get(m1).getProductivity(t4));		
		System.out.println("Membro " + m1.getName() + ": Índice em t4: " + monthSchedule.getTimeTable().get(m1).getMember().getIndex(t4));
		System.out.println("Membro " + m2.getName() + ": Produtividade t4: " + monthSchedule.getTimeTable().get(m2).getProductivity(t4));
		System.out.println("Membro " + m2.getName() + ": Índice em t4: " + monthSchedule.getTimeTable().get(m2).getMember().getIndex(t4));

		System.out.println();		
		System.out.println("Qualidade da agenda mensal de " +m1.getName() + " : " + monthSchedule.getTimeTable().get(m1).getQuality());
		System.out.println("Qualidade da agenda mensal de " +m2.getName() + " : " + monthSchedule.getTimeTable().get(m2).getQuality());
		System.out.println("Qualidade da agenda mensal da equipe: " + monthSchedule.getQuality());


	}

}