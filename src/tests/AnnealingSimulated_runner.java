package tests;

import heuristics.AnnealingSimulated;
import heuristics.RandomScheduling;

import java.io.IOException;

import model.Area;
import model.Member;
import model.Task;
import model.Team;
import schedules.TeamMonthlySchedule;
import useful.FileRead;

public class AnnealingSimulated_runner {

	public static void main(String[] args) {
		Team team = new Team();	//Novo Time
		Task tasks[] = new Task[0];	//Array de Tarefas
		Area areas[] = new Area[0]; //Array de Areas

		try{
			
			String filePath = "instances/d2-a6-t09.txt";
			
			if (args.length > 0)
				filePath = args[0];			
			
			//Carregar o arquivo
			FileRead readed_file = new FileRead(filePath);
			team = readed_file.readMembers();
			tasks = readed_file.readTasks();
			areas = readed_file.readAreas();
			readed_file.setAvailableTasksToMembers();
			readed_file.closefile();			
		} catch (IOException i) { i.printStackTrace();}		

		TeamMonthlySchedule monthSchedule = new TeamMonthlySchedule(team);
		monthSchedule = RandomScheduling.createRandomScheduling(team, areas);
		//System.out.println("Pop. inicial:");
		//monthSchedule.show();
		
		AnnealingSimulated.exec(monthSchedule, areas, 100, 0.8, 1, 0.0001, 1000, 20);

		monthSchedule.show();
		System.out.println();				

		monthSchedule.updateCalcsQuality(team, areas);
		
		for(int i = 0; i < tasks.length; i++)
			System.out.println("Dados da tarefa id=" + tasks[i].getId() + ": Volume:" + tasks[i].getBulkWorked()+ " Indice: " + tasks[i].getIndex());
		
		for (Member m : team.getMembers()){
			System.out.println("Dados do membro: " + m.getId() + " qualidade da agenda mensal: " + monthSchedule.getTimeTable().get(m).getQuality());
		}
		
		System.out.println("Qualidade da agenda do time: " + monthSchedule.getQuality());

	}

}
