package tests;

import heuristics.RandomScheduling;

import java.io.IOException;

import model.Area;
import model.Member;
import model.Task;
import model.Team;
import schedules.MemberMonthlySchedule;
import schedules.TeamMonthlySchedule;
import useful.FileRead;

public class RandomScheduling_runner {		
	

	public static void main(String[] args) {
		Team team = new Team();	//Novo Time
		Task tasks[] = new Task[0];	//Array de Tarefas
		Area areas[] = new Area[0]; //Array de Areas				

		try{
			String filePath = "instances/d2-a2-t06.txt";			
			
			//Carregar o arquivo
			FileRead readed_file = new FileRead(filePath);
			team = readed_file.readMembers();
			tasks = readed_file.readTasks();
			areas = readed_file.readAreas();
			readed_file.setAvailableTasksToMembers();
			readed_file.closefile();			
		} catch (IOException i) { i.printStackTrace();}
		
	
		TeamMonthlySchedule monthSchedule = RandomScheduling.createRandomScheduling(team, areas);
		monthSchedule.show();
		System.out.println();		

		monthSchedule.updateCalcsQuality(team, areas);
		
		int c = 0;
		for (Area a: areas){
			c++;
			System.out.print("Área: "+ c + " Tarefas: ");
			for (Task t: a.getTasks())
				System.out.print("T" + t.getId() + "\t");
			System.out.println();
		}
		
		for (Member m: team.getMembers()){
			System.out.print("Membro " + m.getId() + " Tarefas:");
			for (Task t: m.getAvailableTasks())
				System.out.print("\tT" + t.getId() + "\t");
			System.out.println();
		}
		
		for(int i = 0; i < tasks.length; i++){
			System.out.println("Dados da tarefa id=" + tasks[i].getId() + ": Volume:" + tasks[i].getBulkWorked() + " Custo: " + tasks[i].getCost() + " Indice: " + tasks[i].getIndex());
			for (Member m: team.getMembers())
				for (MemberMonthlySchedule month : monthSchedule.getTimeTable().values())
					if (month.getMember().equals(m))
						System.out.println("Dados do membro M" + m.getId() + " Produtividade: " + month.getProductivity(tasks[i]) + " Volume: " + month.getBulkWorked(tasks[i]));
		}
		
		
		
		System.out.println("\nQualidade da agenda: " + monthSchedule.getQuality());
	}

}
