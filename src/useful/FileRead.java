package useful;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import model.Area;
import model.Member;
import model.Task;
import model.Team;

public class FileRead {

	private int numberOfMembers;
	private int numberOfAreas;
	private int numberofTasks;
	private FileReader fr;
	private BufferedReader br;
	private Team team;	
	
	private Member m[];
	private Task t[];
	private Area areas[];

	/* ***************************************************************************************
	 *      						       								                      CONSTRUTOR			 												                  * 				
	 * *************************************************************************************** */	
	
	public FileRead(String filePath) throws FileNotFoundException{
		this.fr = new FileReader(filePath);
		this.br = new BufferedReader(fr);
		this.team = new Team();
	}
	
	/* ***************************************************************************************
	 *      						       								                      METODO DE CARREGAMENTO DOS DADOS			 												                  * 				
	 * *************************************************************************************** */
	
	public Team readMembers() throws IOException{
		
		//Read the number of members
		String linha = br.readLine();
		numberOfMembers = Integer.parseInt(linha);
		
		System.out.println("\nNumero de Membros: " + linha);//
		System.out.println();//
		
		//Set an Array of Members
		m = new Member[numberOfMembers];
		
		System.out.println("Membros:");//
		for(int i = 0; i < numberOfMembers; i++){
			linha = br.readLine();
			System.out.println(linha);
			String[] parts = linha.split("\t");
			int id = Integer.parseInt(parts[1]);
			String name = parts[2];
			m[i] = new Member(id, name);
			team.addMember(m[i]);
		}
		return this.team;
	}
	
	public Task[] readTasks() throws IOException{
		
		//Read the number of tasks
		String linha = br.readLine();
		numberofTasks = Integer.parseInt(linha);
		
		System.out.println("\nNumero de Tarefas: " + linha);//
		System.out.println();//

		//Set an Array of Tasks
		System.out.println("Tarefas:");//
		t = new Task[numberofTasks];
		for(int i = 0; i < numberofTasks; i++){
			linha = br.readLine();
			System.out.println(linha);
			String[] parts = linha.split("\t");
			t[i] = new Task(i+1, Double.parseDouble(parts[1]),Integer.parseInt(parts[2]),Double.parseDouble(parts[3]),Integer.parseInt(parts[4]),Double.parseDouble(parts[5]));
		}
		return this.t;
	}
	
	public Area[] readAreas() throws IOException{
		
		//Read the number of areas
		String linha = br.readLine();
		numberOfAreas = Integer.parseInt(linha);
		
		System.out.println("\nNumero de Areas: " + linha);//
		System.out.println();//
		
		//Set an Array of Areas
		System.out.println("Areas:");//
		areas = new Area[numberOfAreas];
		for(int i = 0; i < numberOfAreas; i++){
			linha = br.readLine();
			System.out.println(linha);
			String[] parts = linha.split("\t");
			areas[i] = new Area();
			for (String s: parts){
				if (s.charAt(0) == 'T')
					areas[i].addTask(t[Integer.parseInt(s.substring(1))-1]);
			}				
		}
		return this.areas;
	}
	
	public void setAvailableTasksToMembers() throws IOException{
		
		//Set Available Tasks To each Member
		System.out.println();//
		System.out.println("Tarefas associadas aos desenvolvedores:");//
		
		String linha;
		
		for(int i = 0; i < numberOfMembers; i++){
			linha = br.readLine();
			System.out.println(linha);
			String[] parts = linha.split("\t");
			for (String s: parts){
				if (s.charAt(0) == 'T')
					m[i].setAvailableTask(t[Integer.parseInt(s.substring(1))-1]);
			}		    				
		}
	}
	
	public Member[] getMembers(){
		return this.m;
	}
	
	/* ***************************************************************************************
	 *      						       								                      FECHAR ARQUIVO			 												                  * 				
	 * *************************************************************************************** */
	public void closefile() throws IOException{
		//Close the File
		br.close();
		fr.close();
	}	
}
