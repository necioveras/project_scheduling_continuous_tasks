package useful;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Random;

public class FileCreator {
	
	private int numberOfMembers;
	private int numberOfAreas;
	private int numberofTasks;
	private File file;
	private FileWriter fw;
	private BufferedWriter bw;
	
	/* ***************************************************************************************
	 *      						       								                      CONSTRUTOR			 												                  * 				
	 * *************************************************************************************** */
	
	public FileCreator(int numberOfMembers, int numberOfAreas, int numberofTasks, String filePath) throws IOException{
		
		 this.numberOfAreas = numberOfAreas;
		 this.numberOfMembers = numberOfMembers;
		 this.numberofTasks = numberofTasks;
		 this.file = new File(filePath);
		 this.fw = new FileWriter(this.file);
		 this.bw = new BufferedWriter(this.fw);		 
	}
	
	/* ***************************************************************************************
	 *      						       								                      METODO DE ESCRITA DE DADOS 			 												                  * 				
	 * *************************************************************************************** */
	
	public void writeMembers() throws IOException{
		
		//Write the number of members
		bw.write(numberOfMembers + "\n");
		//List the members
		for(int i = 1; i <= numberOfMembers; i++){
			bw.write("D" + i + "\t" + i + "\tMember_Name_" + i + "\n");			
		}		
	}
	
	public void writeTasks() throws IOException{
		
		Random cost = new Random();
		Random delta = new Random();
		Random goal = new Random();
		Random utility = new Random();
		
		//Write the numer of tasks
		bw.write(numberofTasks + "\n");
		//List The tasks with random parameters
		for(int i = 1; i <= numberofTasks; i++){
			int Cost = cost.nextInt(10) + 1;
			int Delta = delta.nextInt(40) + 1;
			int Goal = goal.nextInt(101);
			int Utility = utility.nextInt(40) + 1;
			bw.write("T" + i + "\t" + Cost + "\t" + Delta + "\t" + Goal + "\t" + Utility + "\t" + 0 + "\n");			
		}	
	}
	
	public void writeAreas() throws IOException{
		
		Random tasks = new Random();
		
		//Write the number of Areas
		bw.write(numberOfAreas + "\n");
		//List the Areas and Associated tasks with it
		for(int i = 1; i <= numberOfAreas; i++){
			bw.write("A" + i + "\t");
			int Atasks = tasks.nextInt(numberofTasks) + 1;
			//Task of Areas
			for(int k = 1; k <= Atasks; k++){
				bw.write("T" + k + "\t");
			}
			bw.write("\n");			
		}
	}
	
	public void setAvailableTasksToMembers() throws IOException{
		
		//Members and their Tasks
		for(int i = 1; i <= numberOfMembers; i++){
			bw.write("D" + i + "\t");
			for(int k = 1; k <= numberofTasks; k++){
				bw.write("T" + k + "\t");
			}
			bw.write("\n");			
		}
	}
	
	public void writeAll() throws IOException{
		writeMembers();
		writeTasks();
		writeAreas();
		setAvailableTasksToMembers();
		closefile();
	}
	
	/* ***************************************************************************************
	 *      						       								                      FECHAR ARQUIVO			 												                  * 				
	 * *************************************************************************************** */
	public void closefile() throws IOException{
		//Close the File
		bw.close();
		fw.close();
	}
	
}
