package useful;

public class Definitions {

	public static final int MAX_INSTANT = 8;									//referente ao número máximo de instantes
	public  static final int MAX_DAYS = 5;											//referente ao número máximo de dias para a semana
	
	public static final String[] INSTANCES = {"instances/d2-a2-t06.txt", "instances/d2-a6-t09.txt", "instances/d5-a2-t22.txt"};
	public static final int VALUE_EXEC_DATASETS = 30;                   //referente ao número de execuções para a geração de base de dados estatísticos
	public static final int VALUE_EXEC_MONTHS = 12;                      //referente ao número de meses para a geração de base de dados estatísticos
	public static final int rateEvolutionIndex = 8;                      //referente à taxa de crescimento dos índices para meses subsequentes

}
