package control;

import java.util.LinkedList;
import java.util.List;

import model.Area;
import model.Team;

public class Scheduling {
	
	/* ***************************************************************************************
	 *      						       Variáveis que definem o tamanho dos conjuntos										                     *  				
	 * *************************************************************************************** */
	
	private int SIZE_TEAM = 6;  
	private int SIZE_AREA  = 10;
	private int SIZE_TASKS = 7;    //supoe-se que TODAS as áreas terão inicialmente a mesma quantidade de tarefas
	
	
	
	/* ***************************************************************************************
	 *      						       															Conjuntos										 												 * 				
	 * *************************************************************************************** */	
	private Team t;
	private List<Area> areas;
	
	
	/* ***************************************************************************************
	 *      						       															Métodos Públicos  			 												         * 				
	 * *************************************************************************************** */
	public Scheduling(){
		initialize_sets();
	}
	
		
	/* ***************************************************************************************
	 *      						       															Métodos Privados  			 												         * 				
	 * *************************************************************************************** */
	private void initialize_sets(){
		 t = new  Team(SIZE_TEAM);
		 
		 areas  = new LinkedList<Area>();
		 for (int i = 0; i < SIZE_AREA; i++)
			 areas.add(new Area(SIZE_TASKS));
	}
	
	/* ***************************************************************************************
	 *      						       ------------------------------------------------------------------------------------------										         * 				
	 * *************************************************************************************** */
	
}
