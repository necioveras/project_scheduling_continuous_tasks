Autor: Nécio de Lima Veras

Descrição informal:
Membros de uma equipe que realizam atividades rotineiras e constantes precisam determinar suas atividades diárias em uma agenda de tarefas. Este projeto visa formular um modelo de planejamento inteligente de uma agenda dinâmica de atividades de forma a otimizar metas de produtividade em um período de curto, médio e longo prazo.


Em docs estão os documentos referentes aos detalhamentos do projeto, tais como, formulação matemática, experimentos, modelos conceituais, ...
