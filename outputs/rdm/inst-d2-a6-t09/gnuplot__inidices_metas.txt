set terminal wxt enhanced

set title "Cumprimento de metas (d2-a6-t09-rdm)"
set xlabel "Meses"
set ylabel "Indices X Metas"
set key outside top right

plot 'd2-a6-t09-rdm.txt' u 1 with linespoints title 'T1'; 
replot 'd2-a6-t09-rdm.txt' u 2 with linespoints title 'G1';
replot 'd2-a6-t09-rdm.txt' u 3 with linespoints title 'T2';
replot 'd2-a6-t09-rdm.txt' u 4 with linespoints title 'G2';
replot 'd2-a6-t09-rdm.txt' u 5 with linespoints title 'T3';
replot 'd2-a6-t09-rdm.txt' u 6 with linespoints title 'G3';
replot 'd2-a6-t09-rdm.txt' u 7 with linespoints title 'T4';
replot 'd2-a6-t09-rdm.txt' u 8 with linespoints title 'G4';
replot 'd2-a6-t09-rdm.txt' u 9 with linespoints title 'T5';
replot 'd2-a6-t09-rdm.txt' u 10 with linespoints title 'G5';
replot 'd2-a6-t09-rdm.txt' u 11 with linespoints title 'T6';
replot 'd2-a6-t09-rdm.txt' u 12 with linespoints title 'G6';
replot 'd2-a6-t09-rdm.txt' u 13 with linespoints title 'T7';
replot 'd2-a6-t09-rdm.txt' u 14 with linespoints title 'G7';
replot 'd2-a6-t09-rdm.txt' u 15 with linespoints title 'T8';
replot 'd2-a6-t09-rdm.txt' u 16 with linespoints title 'G8';
replot 'd2-a6-t09-rdm.txt' u 17 with linespoints title 'T9';
replot 'd2-a6-t09-rdm.txt' u 18 with linespoints title 'G9';


set term png enhanced
set output 'd2-a6-t09-rdm-indices-metas.png'

replot
