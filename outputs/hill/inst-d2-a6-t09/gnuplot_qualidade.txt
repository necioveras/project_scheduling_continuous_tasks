set terminal wxt enhanced

set title "Qualidade das agendas dos membros (d2-a6-t09-hillC)"
set xlabel "Meses"
set ylabel "Valor de qualidade"
set key bottom right

plot 'd2-a6-t09-hillC.txt' u 46 with linespoints title 'QE'; 
replot 'd2-a6-t09-hillC.txt' u 47 with linespoints title 'QM1';
replot 'd2-a6-t09-hillC.txt' u 48 with linespoints title 'QM2';


set term png enhanced
set output 'd2-a6-t09-hillC-qualidade.png'

replot
