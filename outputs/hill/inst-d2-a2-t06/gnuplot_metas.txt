set terminal wxt enhanced

set title "Metas das tarefas (d2-a2-t06-hillC)"
set xlabel "Meses"
set ylabel "Metas"
set key top left

plot 'd2-a2-t06-hillC.txt' u 2 with linespoints title 'G1'; 
replot 'd2-a2-t06-hillC.txt' u 4 with linespoints title 'G2';
replot 'd2-a2-t06-hillC.txt' u 6 with linespoints title 'G3';
replot 'd2-a2-t06-hillC.txt' u 8 with linespoints title 'G4';
replot 'd2-a2-t06-hillC.txt' u 10 with linespoints title 'G5';
replot 'd2-a2-t06-hillC.txt' u 12 with linespoints title 'G6';


set term png enhanced
set output 'd2-a2-t06-hillC-metas.png'

replot
